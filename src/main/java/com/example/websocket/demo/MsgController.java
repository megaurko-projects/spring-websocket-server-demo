package com.example.websocket.demo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;

@Controller
@MessageMapping("/msg")
public class MsgController {
    private static final Logger LOG = LogManager.getLogger(MsgController.class);

    @MessageMapping("/message")
    public void app(@Payload RequestMsg requestMsg) {
        LOG.info("Got stomp message to /msg " + requestMsg);
    }
}
