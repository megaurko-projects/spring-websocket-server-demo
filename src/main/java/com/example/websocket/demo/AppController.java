package com.example.websocket.demo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

@Controller
@MessageMapping("/queue")
public class AppController {
    private static final Logger LOG = LogManager.getLogger(AppController.class);

    @Autowired SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/message")
    @SendToUser(value = "/response/private", broadcast = false)
    public RequestMsg app(@Payload RequestMsg requestMsg, @Header("simpSessionId") String sessionId) {
        LOG.info("Got stomp message to /app " + requestMsg);

        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);
        simpMessagingTemplate.convertAndSendToUser(
                sessionId,
                "/response/private",
                new RequestMsg("Before hello..."),
                headerAccessor.getMessageHeaders());

        return new RequestMsg("Hello from server, user!");
    }
}
