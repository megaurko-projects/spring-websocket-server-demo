package com.example.websocket.demo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTask {
    private static final Logger LOG = LogManager.getLogger(ScheduledTask.class);

    @Autowired SimpMessagingTemplate simpMessagingTemplate;

    @Scheduled(fixedRate = 5000)
    public void sendMessage() {
        RequestMsg msg = new RequestMsg("Dat be good choice man!");
        LOG.info("sending message: " + msg);
        simpMessagingTemplate.convertAndSend("/dst/broadcast", msg);
    }
}
